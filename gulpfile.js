let gulp         = require('gulp'),
    browserSync  = require('browser-sync'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCss     = require('gulp-clean-css'),
    concat       = require('gulp-concat'),
    imagemin     = require('gulp-imagemin'),
    jsminify     = require('gulp-js-minify'),
    sass         = require('gulp-sass'),
    uglify       = require('gulp-uglify-es').default,
    watch        = require('gulp-watch'),
    clean        = require('gulp-clean');

gulp.task('clean', function () {
    return gulp.src('dist')
        .pipe(clean())
});

gulp.task('browser-sync', function () {
    browserSync.init({
        server: {
            baseDir: "./"
        },
        notify: false
    });
});

gulp.task('sass', function() {
    return gulp.src('src/scss/*.scss')
        .pipe(sass().on('error', function (error) {
            console.log(error);
            alert(error);
        }))
        .pipe(gulp.dest('src/css'))
});

gulp.task('css', function() {
    return gulp.src('src/css/*.css')
        .pipe(autoprefixer())
        .pipe(concat('main.css'))
        .pipe(cleanCss())
        .pipe(gulp.dest('dist/css'))
});

gulp.task('js', function() {
    return gulp.src('src/js/index.js')
        .pipe(uglify())
        .pipe(jsminify())
        .pipe(gulp.dest('dist/js'))
});

gulp.task('imagemin', function() {
    return gulp.src('src/img/*.png')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/img'))
});

gulp.task('watch', function () {
    gulp.watch('dist', gulp.parallel('clean'))
    gulp.watch('src/scss/*.scss', gulp.parallel('sass'))
    gulp.watch('src/css/*.css', gulp.parallel('css'))
    gulp.watch('src/js/index.js', gulp.parallel('js'))
    gulp.watch('src/img/.jpg', gulp.parallel('imagemin'))
});

exports.build = gulp.parallel('sass', 'css', 'js', 'imagemin');
exports.dev = gulp.parallel('browser-sync', 'watch');