/**
 * @desc Hover for logotype
 * @desc Location is "Header"
 * @type {Element}
 */
let logotype = document.querySelector('.logotype');
let logoUnHover = document.querySelector('.logotype-img');
let logoHover = document.querySelector('.logotype-img-hover');
let logoTxt = document.querySelector('.logotype-txt');

logotype.addEventListener('mouseover', function () {
    logoUnHover.style.display = 'none';
    logoHover.style.display = 'block';
    logoTxt.style.color = '#FFFFFF';
});

logotype.addEventListener('mouseleave', function () {
    logoUnHover.style.display = 'block';
    logoHover.style.display = 'none';
    logoTxt.style.color = '#8D81AC';
});

/**
 * @desc Show or hide dropdown menu
 * @desc Location is "Fork App"
 * @type {Element}
 */
let dropBtnMenu = document.querySelector('.fa-bars');
let dropMenu = document.querySelector('.fork-app-drop-down-menu');
let btnMenuClicked = 0;
let forkAppHeadings = document.querySelector('.fork-app-headings');
let downloadForFree = document.querySelector('.download-for-free-now');

dropBtnMenu.addEventListener('click', function () {
    btnMenuClicked += 1;

    if(btnMenuClicked == 2) {btnMenuClicked = 0}

    if(btnMenuClicked == 0) {
        dropMenu.style.display = 'none';
        forkAppHeadings.style.display = 'block';
        downloadForFree.style.display = 'block';
    }
    if(btnMenuClicked == 1) {
        dropMenu.style.display = 'block';
        forkAppHeadings.style.display = 'none';
        downloadForFree.style.display = 'none';
    }
});